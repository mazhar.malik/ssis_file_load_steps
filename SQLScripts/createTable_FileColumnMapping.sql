USE [Fusion]
GO

/****** Object:  Table [dbo].[FileColumnMapping]    Script Date: 6/27/2019 4:12:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FileColumnMapping](
	[MapId] [int] IDENTITY(1,1) NOT NULL,
	[DatasetTable] [varchar](200) NOT NULL,
	[MapFromField] [varchar](100) NOT NULL,
	[MapToField] [varchar](100) NOT NULL,
 CONSTRAINT [PK_FileColumnMapping] PRIMARY KEY CLUSTERED 
(
	[DatasetTable] ASC,
	[MapFromField] ASC,
	[MapToField] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


