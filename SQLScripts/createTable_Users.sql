USE [Fusion]
GO

/****** Object:  Table [raw].[Users]    Script Date: 7/1/2019 11:08:35 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Users](
	[UserId] [nvarchar](4000) NULL,
	[UserName] [nvarchar](4000) NULL,
	[OrgDefinedId] [nvarchar](4000) NULL,
	[FirstName] [nvarchar](4000) NULL,
	[MiddleName] [nvarchar](4000) NULL,
	[LastName] [nvarchar](4000) NULL,
	[IsActive] [nvarchar](4000) NULL,
	[Organization] [nvarchar](4000) NULL,
	[InternalEmail] [nvarchar](4000) NULL,
	[ExternalEmail] [nvarchar](4000) NULL,
	[SignupDate] [nvarchar](4000) NULL,
	[FirstLoginDate] [nvarchar](4000) NULL,
	[Version] [nvarchar](4000) NULL
) ON [PRIMARY]
GO


