USE [Fusion]
GO

/****** Object:  Table [dbo].[OrgUnits]    Script Date: 6/27/2019 4:13:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrgUnits](
	[OrgUnitId] [nvarchar](4000) NULL,
	[Organization] [nvarchar](4000) NULL,
	[Type] [nvarchar](4000) NULL,
	[Name] [nvarchar](4000) NULL,
	[Code] [nvarchar](4000) NULL,
	[StartDate] [nvarchar](4000) NULL,
	[EndDate] [nvarchar](4000) NULL,
	[IsActive] [nvarchar](4000) NULL,
	[CreatedDate] [nvarchar](4000) NULL,
	[IsDeleted] [nvarchar](4000) NULL,
	[DeletedDate] [nvarchar](4000) NULL,
	[RecycledDate] [nvarchar](4000) NULL,
	[Version] [nvarchar](4000) NULL
) ON [PRIMARY]
GO


