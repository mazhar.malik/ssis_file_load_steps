/****** Mapping For Table OrgUnits ******/
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'OrgUnitId', 'OrgUnitId');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'Organization', 'Organization');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'Type', 'Type');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'Name', 'Name');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'Code', 'Code');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'StartDate', 'StartDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'EndDate', 'EndDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'IsActive', 'IsActive');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'CreatedDate', 'CreatedDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'IsDeleted', 'IsDeleted');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'DeletedDate', 'DeletedDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'RecycledDate', 'RecycledDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('OrgUnits', 'Version', 'Version');
