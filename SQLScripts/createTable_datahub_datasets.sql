USE [Fusion]
GO

/****** Object:  Table [dbo].[DATAHUB_DATASETS]    Script Date: 6/27/2019 4:10:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DATAHUB_DATASETS](
	[DatasetUniqueId] [int] identity (1,1) NOT NULL,
	[DatasetName] [varchar](100) NOT NULL,
	[DatasetFileName] [varchar](100) NOT NULL,
	[DatasetDesc] [varchar](500) NULL,
	[DatasetTable] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[DatasetUniqueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


