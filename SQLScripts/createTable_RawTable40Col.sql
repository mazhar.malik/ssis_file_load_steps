USE [Fusion]
GO

/****** Object:  Table [dbo].[RawTable40Col]    Script Date: 6/27/2019 4:12:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RawTable40Col](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Column0] [nvarchar](4000) NULL,
	[Column1] [nvarchar](4000) NULL,
	[Column2] [nvarchar](4000) NULL,
	[Column3] [nvarchar](4000) NULL,
	[Column4] [nvarchar](4000) NULL,
	[Column5] [nvarchar](4000) NULL,
	[Column6] [nvarchar](4000) NULL,
	[Column7] [nvarchar](4000) NULL,
	[Column8] [nvarchar](4000) NULL,
	[Column9] [nvarchar](4000) NULL,
	[Column10] [nvarchar](4000) NULL,
	[Column11] [nvarchar](4000) NULL,
	[Column12] [nvarchar](4000) NULL,
	[Column13] [nvarchar](4000) NULL,
	[Column14] [nvarchar](4000) NULL,
	[Column15] [nvarchar](4000) NULL,
	[Column16] [nvarchar](4000) NULL,
	[Column17] [nvarchar](4000) NULL,
	[Column18] [nvarchar](4000) NULL,
	[Column19] [nvarchar](4000) NULL,
	[Column20] [nvarchar](4000) NULL,
	[Column21] [nvarchar](4000) NULL,
	[Column22] [nvarchar](4000) NULL,
	[Column23] [nvarchar](4000) NULL,
	[Column24] [nvarchar](4000) NULL,
	[Column25] [nvarchar](4000) NULL,
	[Column26] [nvarchar](4000) NULL,
	[Column27] [nvarchar](4000) NULL,
	[Column28] [nvarchar](4000) NULL,
	[Column29] [nvarchar](4000) NULL,
	[Column30] [nvarchar](4000) NULL,
	[Column31] [nvarchar](4000) NULL,
	[Column32] [nvarchar](4000) NULL,
	[Column33] [nvarchar](4000) NULL,
	[Column34] [nvarchar](4000) NULL,
	[Column35] [nvarchar](4000) NULL,
	[Column36] [nvarchar](4000) NULL,
	[Column37] [nvarchar](4000) NULL,
	[Column38] [nvarchar](4000) NULL,
	[Column39] [nvarchar](4000) NULL,
 CONSTRAINT [PK_RawTable40ColGroup1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


