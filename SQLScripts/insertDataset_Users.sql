USE [Fusion]
GO

INSERT INTO [dbo].[DATAHUB_DATASETS]
           ([DatasetName]
           ,[DatasetFileName]
           ,[DatasetDesc]
           ,[DatasetTable])
     VALUES
           ('Users'
           ,'users'
           ,'The users dataset returns a list of users in your organization.'
           ,'Users')
GO


