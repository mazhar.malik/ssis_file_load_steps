/****** Mapping For Table Users ******/
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'UserId', 'UserId');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'UserName', 'UserName');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'OrgDefinedId', 'OrgDefinedId');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'FirstName', 'FirstName');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'MiddleName', 'MiddleName');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'LastName', 'LastName');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'IsActive', 'IsActive');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'Organization', 'Organization');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'InternalEmail', 'InternalEmail');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'ExternalEmail', 'ExternalEmail');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'SignupDate', 'SignupDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'FirstLoginDate', 'FirstLoginDate');
INSERT INTO [Fusion].[dbo].[FileColumnMapping] ([DatasetTable], [MapFromField] ,[MapToField]) VALUES ('Users', 'Version', 'Version');
