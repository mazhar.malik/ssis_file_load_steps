USE [Fusion]
GO

INSERT INTO [dbo].[DATAHUB_DATASETS]
           ([DatasetName]
           ,[DatasetFileName]
           ,[DatasetDesc]
           ,[DatasetTable])
     VALUES
           ('Org Units'
           ,'org_units'
           ,'The org units dataset returns details about all org units within your organization.'
           ,'OrgUnits')
GO


